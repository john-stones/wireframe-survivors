extends Node


signal player_colour_changed(colour: Color)
signal gun_colour_changed(colour: Color)


var player_colour := Color.DARK_CYAN:
	set(value):
		player_colour = value
		self.player_colour_changed.emit(value)

var gun_colour := Color.CYAN:
	set(value):
		gun_colour = value
		self.gun_colour_changed.emit(value)
