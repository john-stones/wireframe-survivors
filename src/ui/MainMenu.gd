extends CanvasLayer


const GAME_PATH := "res://src/World.tscn"


func start_game():
	self.get_tree().change_scene_to_file(self.GAME_PATH)


func quit():
	self.get_tree().quit()
