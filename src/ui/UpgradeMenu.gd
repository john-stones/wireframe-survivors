extends CanvasLayer
class_name UpgradeMenu


signal upgrade_selected(gun: int, upgrade: int)

@onready var _gun_left_damage_label := \
		$Menu/GunStats/GunStatsLeft/Rows/GridContainer/Damage as Label
@onready var _gun_left_firerate_label := \
		$Menu/GunStats/GunStatsLeft/Rows/GridContainer/Firerate as Label
@onready var _gun_left_pierce_label := \
		$Menu/GunStats/GunStatsLeft/Rows/GridContainer/Pierce as Label
@onready var _gun_left_spread_label := \
		$Menu/GunStats/GunStatsLeft/Rows/GridContainer/Spread as Label
@onready var _gun_left_shots_label := \
		$Menu/GunStats/GunStatsLeft/Rows/GridContainer/Shots as Label

@onready var _gun_right_damage_label := \
		$Menu/GunStats/GunStatsRight/Rows/GridContainer/Damage as Label
@onready var _gun_right_firerate_label := \
		$Menu/GunStats/GunStatsRight/Rows/GridContainer/Firerate as Label
@onready var _gun_right_pierce_label := \
		$Menu/GunStats/GunStatsRight/Rows/GridContainer/Pierce as Label
@onready var _gun_right_spread_label := \
		$Menu/GunStats/GunStatsRight/Rows/GridContainer/Spread as Label
@onready var _gun_right_shots_label := \
		$Menu/GunStats/GunStatsRight/Rows/GridContainer/Shots as Label

@onready var _gun_buttons := [
	$Menu/GunStats/GunStatsLeft/Button as Button,
	$Menu/GunStats/GunStatsRight/Button as Button,
] as Array[Button]

@onready var _upgrade_1_name := \
		$Menu/Upgrades/Upgrade1/Rows/Title as Label
@onready var _upgrade_1_description := \
		$Menu/Upgrades/Upgrade1/Rows/Description as Label

@onready var _upgrade_2_name := \
		$Menu/Upgrades/Upgrade2/Rows/Title as Label
@onready var _upgrade_2_description := \
		$Menu/Upgrades/Upgrade2/Rows/Description as Label

@onready var _upgrade_3_name := \
		$Menu/Upgrades/Upgrade3/Rows/Title as Label
@onready var _upgrade_3_description := \
		$Menu/Upgrades/Upgrade3/Rows/Description as Label

@onready var _upgrade_buttons := [
	$Menu/Upgrades/Upgrade1/Button as Button,
	$Menu/Upgrades/Upgrade2/Button as Button,
	$Menu/Upgrades/Upgrade3/Button as Button,
] as Array[Button]

@export_group("Left Gun")
@export
var gun_left_damage := 0:
	set(value):
		gun_left_damage = value
		self._gun_left_damage_label.text = str(gun_left_damage)

@export
var gun_left_firerate := 0.0:
	set(value):
		gun_left_firerate = value
		self._gun_left_firerate_label.text = str(gun_left_firerate)

@export
var gun_left_pierce := 0:
	set(value):
		gun_left_pierce = value
		self._gun_left_pierce_label.text = str(gun_left_pierce)

@export
var gun_left_spread := 0.0:
	set(value):
		gun_left_spread = value
		self._gun_left_spread_label.text = str(gun_left_spread)

@export
var gun_left_shots := 0:
	set(value):
		gun_left_shots = value
		self._gun_left_shots_label.text = str(gun_left_shots)

@export_group("Right Gun")
@export
var gun_right_damage := 0:
	set(value):
		gun_right_damage = value
		self._gun_right_damage_label.text = str(gun_right_damage)

@export
var gun_right_firerate := 0.0:
	set(value):
		gun_right_firerate = value
		self._gun_right_firerate_label.text = str(gun_right_firerate)

@export
var gun_right_pierce := 0:
	set(value):
		gun_right_pierce = value
		self._gun_right_pierce_label.text = str(gun_right_pierce)

@export
var gun_right_spread := 0.0:
	set(value):
		gun_right_spread = value
		self._gun_right_spread_label.text = str(gun_right_spread)

@export
var gun_right_shots := 0:
	set(value):
		gun_right_shots = value
		self._gun_right_shots_label.text = str(gun_right_shots)

@export_group("Upgrade 1")
@export
var upgrade_1_name := "Upgrade 1":
	set(value):
		upgrade_1_name = value
		self._upgrade_1_name.text = upgrade_1_name

@export
var upgrade_1_description := "Upgrade 1":
	set(value):
		upgrade_1_description = value
		self._upgrade_1_description.text = upgrade_1_description

@export_group("Upgrade 2")
@export
var upgrade_2_name := "Upgrade 2":
	set(value):
		upgrade_2_name = value
		self._upgrade_2_name.text = upgrade_2_name

@export
var upgrade_2_description := "Upgrade 2":
	set(value):
		upgrade_2_description = value
		self._upgrade_2_description.text = upgrade_2_description

@export_group("Upgrade 3")
@export
var upgrade_3_name := "Upgrade 3":
	set(value):
		upgrade_3_name = value
		self._upgrade_3_name.text = upgrade_3_name

@export
var upgrade_3_description := "Upgrade 3":
	set(value):
		upgrade_3_description = value
		self._upgrade_3_description.text = upgrade_3_description


func select_upgrade():
	# TODO: I'm doing this shit old school because I'm done thinking
	var gun_button_i := -1
	for i in range(self._gun_buttons.size()):
		if self._gun_buttons[i].button_pressed:
			gun_button_i = i
	
	var upgrade_button_i := -1
	for i in range(self._upgrade_buttons.size()):
		if self._upgrade_buttons[i].button_pressed:
			upgrade_button_i = i
	
	if gun_button_i >= 0 and upgrade_button_i >= 0:
		self.upgrade_selected.emit(gun_button_i, upgrade_button_i)
		self._gun_buttons.all(func(button: Button):
			button.button_pressed = false)
		self._upgrade_buttons.all(func(button: Button):
			button.button_pressed = false)
