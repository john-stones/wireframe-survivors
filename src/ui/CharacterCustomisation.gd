extends CanvasLayer


signal back_pressed

@onready var player_colour_picker: ColorPicker = \
		$Options/PlayerColourRow/PlayerColorPicker
@onready var gun_colour_picker: ColorPicker = \
		$Options/GunColourRow/GunColorPicker


func _ready():
	self.player_colour_picker.color_changed.connect(self.update_player_colour)
	self.gun_colour_picker.color_changed.connect(self.update_gun_colour)


func update_player_colour(colour: Color):
	GlobalProperties.player_colour = colour


func update_gun_colour(colour: Color):
	GlobalProperties.gun_colour = colour
