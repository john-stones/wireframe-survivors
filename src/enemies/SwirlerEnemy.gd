extends Enemy
class_name SwirlerEnemy


@onready var attack_area: Area2D = $AttackArea

@export var flank_angle_min := 55
@export var flank_angle_max := 35

@export var target: Node2D

@export var speed := 150

var flank_angle: float


func _ready():
	super._ready()
	self.attack_area.connect("body_entered", self.attack)
	
	var flank_clockwise := randi_range(0, 1) == 1
	if flank_clockwise:
		self.flank_angle = \
				randf_range(self.flank_angle_min, self.flank_angle_max)
	else:
		self.flank_angle = \
				-randf_range(self.flank_angle_min, self.flank_angle_max)


func _process(delta):
	if is_instance_valid(self.target):
		self.velocity = self.global_position \
					.direction_to(self.target.global_position) \
					.rotated(deg_to_rad(self.flank_angle)) \
					* speed
	
	self.rotation = self.velocity.angle()
	self.move_and_slide()
