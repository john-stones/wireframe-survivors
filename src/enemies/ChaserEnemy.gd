extends Enemy
class_name ChaserEnemy


@onready var attack_area: Area2D = $AttackArea

@export var target: Node2D

@export var speed := 120

@export var spin_speed := 4


func _ready():
	super._ready()
	self.attack_area.connect("body_entered", self.attack)
	$AnimationPlayer.play("enemy/spin")


func _process(_delta):
	if is_instance_valid(self.target):
		self.velocity = self.global_position \
					.direction_to(self.target.global_position) * speed
	
	self.move_and_slide()
