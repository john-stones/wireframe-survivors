extends Enemy
class_name BroodEnemy


@onready var skin := $Skin
@onready var attack_area: Area2D = $AttackArea

@export var baby_enemy_scene: PackedScene
@export var baby_spawn_count: = 3
@export var baby_spawn_angle_min: = deg_to_rad(-90)
@export var baby_spawn_angle_max: = deg_to_rad(90)

@export var target: Node2D

@export var speed := 60

@export var spin_speed := 2


func _ready():
	super._ready()
	self.attack_area.connect("body_entered", self.attack)


func _process(delta):
	if is_instance_valid(self.target):
		self.velocity = self.global_position \
					.direction_to(self.target.global_position) * speed
	
	self.skin.rotate(spin_speed * delta)
	self.move_and_slide()


func _on_kill():
	var direction_away_target = self.global_position \
			.direction_to(self.target.global_position) \
			.rotated(TAU / 2)
	
	for i in range(self.baby_spawn_count):
		var baby_enemy = self.baby_enemy_scene.instantiate() as BabyEnemy
		var baby_enemy_direction = direction_away_target \
				.rotated(randf_range(baby_spawn_angle_min, baby_spawn_angle_max))
		baby_enemy.velocity = baby_enemy_direction * baby_enemy.speed_max
		baby_enemy.position = self.position
		baby_enemy.target = self.target
		self.get_parent().add_child.call_deferred(baby_enemy)
	
	self._death_sound_player.reparent(self.get_parent())
	self._death_sound_player.play()
