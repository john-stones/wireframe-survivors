extends Enemy
class_name BabyEnemy


@onready var attack_area: Area2D = $AttackArea

@export var target: Node2D

@export var acceleration := 500
@export var speed_max := 150

@export var spin_speed := 2


func _ready():
	super._ready()
	self.attack_area.connect("body_entered", self.attack)


func _process(delta):
	if is_instance_valid(self.target):
		var new_velocity = self.velocity \
				+ self.global_position.direction_to(self.target.global_position) \
				* acceleration \
				* delta
		self.velocity = new_velocity.limit_length(speed_max)
	
	self.rotation = self.velocity.angle()
	self.move_and_slide()
