extends CharacterBody2D
class_name Enemy


signal killed(enemy: Enemy)

const DeathSound: AudioStream = preload("res://assets/audio/Retro Impact Punch 07.wav")
const AudioStreamPlayerOneOff: Script = preload("res://src/AudioStreamPlayerOneOff.gd")

var health_max := 48
var health := self.health_max: set = _set_health

var _death_sound_player: AudioStreamPlayerOneOff


func _ready():
	self.add_to_group("enemies", true)
	
	var death_sound_player := AudioStreamPlayer.new()
	death_sound_player.stream = self.DeathSound
	death_sound_player.set_script(AudioStreamPlayerOneOff)
	death_sound_player.bus = "SFX"
	self._death_sound_player = death_sound_player
	self.add_child(death_sound_player)


func attack(player: Node2D):
	if player is Player:
		player.kill()


func _set_health(value):
	health = clamp(value, 0, self.health_max)
	if health <= 0:
		self.kill()


func kill():
	self._on_kill()
	if is_instance_valid(self._death_sound_player):
		self._death_sound_player.reparent(self.get_parent())
		self._death_sound_player.play()
	self.queue_free()
	self.killed.emit(self)


func _on_kill():
	pass
