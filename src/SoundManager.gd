extends Node


const AudioStreamPlayerOneOff: Script = preload("res://src/AudioStreamPlayerOneOff.gd")

@export var hover_button_sound: AudioStream


func play_hover_button():
	var hover_button_player = AudioStreamPlayer.new()
	self.add_child(hover_button_player)
	hover_button_player.set_script(AudioStreamPlayerOneOff)
	hover_button_player.bus = "GUI"
	hover_button_player.stream = self.hover_button_sound
	hover_button_player.play()
