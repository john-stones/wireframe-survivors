extends Node2D
class_name Gun


signal updated(gun: Gun)

const UPGRADE_COUNT_MAX := 5

const DAMAGE_BASE := 12
const FIRERATE_BASE := 0.8
const SPREAD_BASE := 0.0
const BULLET_COUNT_BASE := 1
const PIERCE_BASE := 0
const BULLET_SPEED_BASE := 500.0

const FIRERATE_MIN := 0.05

@onready var skin: Polygon2D = $Skin
@onready var shoot_sound: AudioStreamPlayer = $ShootSound

@export var display_name := "Gun":
	set(value):
		display_name = value
		updated.emit(self)

@export var action := ""

@export var bullet_scene: PackedScene

## The Node that takes ownership of the shot bullets
@export
var bullet_parent: Node2D

## The speed of the bullets
@export var bullet_speed := BULLET_SPEED_BASE

## Distance from origin to the end of the barrel
@export var length := 0

## Angle of the bullet spread
var spread := self.SPREAD_BASE:
	set(value): spread = clamp(value, 0, 360)

## Shot cooldown
var firerate := self.FIRERATE_BASE:
	set(value):
		firerate = max(value, self.FIRERATE_MIN)
		if is_instance_valid(_shoot_timer):
			self._shoot_timer.wait_time = firerate

## Number of bullets per shot
var bullet_count := self.BULLET_COUNT_BASE:
	set(value): bullet_count = max(value, 0)

## Damage of each bullet
var damage := self.DAMAGE_BASE

## Number of hits before bullet is destroyed
var pierce := self.PIERCE_BASE:
	set(value):
		pierce = max(value, 0)

var _upgrades := [] as Array[Upgrade]

var _shoot_timer: Timer


func _init():
	# Default upgrades
	range(UPGRADE_COUNT_MAX) \
		.map(func(_idx): _upgrades.append(null))


func _ready():
	# Firerate timer
	var shoot_timer := Timer.new()
	shoot_timer.name = "ShootTimer"
	shoot_timer.wait_time = self.firerate
	shoot_timer.timeout.connect(func():
		if self.action != "" and Input.is_action_pressed(self.action):
			self.shoot()
		else:
			shoot_timer.stop())
	self.add_child(shoot_timer)
	self._shoot_timer = shoot_timer
	
	# Colour
	self.set_colour(GlobalProperties.gun_colour)
	GlobalProperties.gun_colour_changed.connect(self.set_colour)


func _process(_delta):
	if self._shoot_timer.is_stopped() \
			and self.action != "" \
			and Input.is_action_just_pressed(self.action):
		self.shoot()
		self._shoot_timer.start()


func shoot():
	for i in range(self.bullet_count):
		self.shoot_single_bullet()


func shoot_single_bullet():
	assert(self.bullet_scene)
	
	var bullet_global_position = self.global_position \
			+ Vector2(length, 0).rotated(self.global_rotation)
	
	var bullet_spread_angle = \
			deg_to_rad(randf_range(-self.spread / 2, self.spread / 2))
	
	var bullet_global_direction = Vector2.RIGHT \
			.rotated(self.global_rotation) \
			.rotated(bullet_spread_angle)
	
	var bullet: Bullet = self.bullet_scene.instantiate()
	bullet.global_position = bullet_global_position
	bullet.direction = bullet_global_direction
	bullet.speed = self.bullet_speed
	bullet.damage = self.damage
	bullet.health = self.pierce + 1
	
	if is_instance_valid(self.bullet_parent):
		self.bullet_parent.add_child(bullet)
	else:
		self.get_tree().root.add_child(bullet)
	
	self.shoot_sound.play()


func get_upgrade(idx: int) -> Upgrade:
	return _upgrades[idx] \
		if idx >= 0 and idx < _upgrades.size() \
		else null


func add_upgrade(idx: int, upgrade: Upgrade):
	if idx < 0 or idx >= _upgrades.size(): return
	
	self._upgrades[idx] = upgrade
	self._apply_upgrades()
	self.updated.emit(self)


func remove_upgrade(idx: int):
	self.add_upgrade(idx, null)


func has_upgrade(upgrade: Upgrade):
	return self._upgrades.has(upgrade)


func _apply_upgrades():
	self.damage = self.DAMAGE_BASE
	self.firerate = self.FIRERATE_BASE
	self.spread = self.SPREAD_BASE
	self.bullet_count = self.BULLET_COUNT_BASE
	self.pierce = self.PIERCE_BASE
	self.bullet_speed = self.BULLET_SPEED_BASE
	self._upgrades \
		.filter(func(upgrade): return upgrade != null) \
		.map(func(upgrade): upgrade.apply(self))


func set_colour(value: Color):
	self.skin.color = value
