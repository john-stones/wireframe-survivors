extends CharacterBody2D
class_name Player


@export var speed := 200

@onready var skin := $Skin
@onready var death_sound := $DeathSound

@export var _gun_left_parent: Node2D
@export var _gun_right_parent: Node2D

var _gun_left: Gun
var _gun_right: Gun


func _ready():
	self.set_colour(GlobalProperties.player_colour)
	GlobalProperties.player_colour_changed.connect(self.set_colour)


func _process(_delta):
	var direction := Vector2.ZERO
	if Input.is_action_pressed("player_up"):
		direction += Vector2.UP
	if Input.is_action_pressed("player_down"):
		direction += Vector2.DOWN
	if Input.is_action_pressed("player_left"):
		direction += Vector2.LEFT
	if Input.is_action_pressed("player_right"):
		direction += Vector2.RIGHT

	self.velocity = direction.normalized() * speed
	self.move_and_slide()


func kill():
	if is_instance_valid(death_sound) and not self.death_sound.playing:
		self.death_sound.reparent(self.get_parent())
		self.death_sound.play()
	
	SignalBus.emit_signal("player_killed")
	self.speed = 0
	self.visible = false


func equip_gun_left(gun: Gun):
	unequip_gun_left()
	
	_gun_left = gun
	_gun_left.action = "gun_shoot_left"
	_gun_left_parent.add_child(gun)


func unequip_gun_left():
	if is_instance_valid(_gun_left):
		_gun_left.action = ""
		_gun_left_parent.remove_child(_gun_left)
		_gun_left = null


func equip_gun_right(gun: Gun):
	unequip_gun_right()
	
	_gun_right = gun
	_gun_right.action = "gun_shoot_right"
	_gun_right_parent.add_child(gun)


func unequip_gun_right():
	if is_instance_valid(_gun_right):
		_gun_right.action = ""
		_gun_right_parent.remove_child(_gun_right)
		_gun_right = null


func set_colour(value: Color):
	self.skin.color = value
