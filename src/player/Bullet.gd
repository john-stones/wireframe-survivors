extends Area2D
class_name Bullet


@onready var despawn_timer = $DespawnTimer

var speed := 500.0
var direction := Vector2.RIGHT:
	set(value): direction = value.normalized()

## Damage done on hit
@export_range(0, 0, 1, "or_greater")
var damage := 1

## Number of hits before destroyed
@export_range(0, 0, 1, "or_greater")
var health := 1:
	set(value):
		health = value
		if health <= 0:
			self.queue_free()


func _ready():
	self.add_to_group("bullet")
	self.despawn_timer.connect("timeout", self.queue_free)
	self.body_entered.connect(self.hit)


func _process(delta):
	self.position += direction * speed * delta


func hit(body: CollisionObject2D):
	if body is Enemy:
		body.health += -self.damage
		self.health += -1
	elif body.get_collision_layer_value(5): # If wall
		self.queue_free()
	else:
		push_warning("Bullet hit a body that isn't an Enemy: " + str(body))
