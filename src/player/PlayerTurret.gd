extends Node2D
class_name PlayerTurret


func _process(_delta):
	var mouse_position := self.get_global_mouse_position()
	self.look_at(mouse_position)
