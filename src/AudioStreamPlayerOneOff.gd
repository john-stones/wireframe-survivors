extends AudioStreamPlayer
class_name AudioStreamPlayerOneOff


func _ready():
	self.finished.connect(self.queue_free)
