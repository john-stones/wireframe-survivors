extends Node2D
class_name Stage


signal wave_complete

@onready var player := $Player as Player

@export
var start_time_seconds := 5

var _start_timer: Timer

# TODO: Temporary
var _enemy_count := 5
var _enemy_count_increase := 5


func _ready():
	_start_timer = Timer.new()
	_start_timer.wait_time = start_time_seconds
	_start_timer.one_shot = true
	_start_timer.timeout.connect(spawn_next_wave)
	add_child(_start_timer)
#	_start_timer.start()


func enemy_killed(_enemy: Enemy):
	var enemies := get_tree().get_nodes_in_group("enemies") \
		.filter(func(enemy): return not enemy.is_queued_for_deletion()) \
		as Array
		
	if enemies.is_empty():
		wave_complete.emit()


func spawn_next_wave():
	# TODO: Temp to make infinite spawns
	var enemy_scene := load("res://src/enemies/ChaserEnemy.tscn") as PackedScene
	
	self.spawn_wave([{"scene": enemy_scene, "count": self._enemy_count}])
	self._enemy_count += self._enemy_count_increase


func spawn_wave(wave: Array[Dictionary]):
	wave.reduce(
			func(accum, dict):
				# Dictionary{scene, count} -> Array[Enemy]
				for i in range(dict["count"]):
					var node := dict["scene"].instantiate() as Node
					accum.append(node)
				return accum,
			[]) \
		.filter(func(node): return node is Enemy) \
		.map(self.spawn_enemy)


func spawn_enemy(enemy: Enemy):
	enemy.position = self.enemy_spawn_location()
	enemy.target = self.player
	enemy.killed.connect(enemy_killed)
	self.add_child(enemy)


func enemy_spawn_location() -> Vector2:
	var spawn_distance := self.get_viewport_rect().position \
			.distance_to(self.get_viewport_rect().get_center())
	return self.get_viewport().get_camera_2d().get_screen_center_position() \
			+ (Vector2.RIGHT * spawn_distance).rotated(TAU * randf())
