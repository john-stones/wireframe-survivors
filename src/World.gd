extends Node2D


@export var main_menu_scene: PackedScene

@export var gun_default: PackedScene

@onready var stage: Stage = $Stage
@onready var pause_menu: CanvasLayer = $PauseMenu

@onready var game_over_menu: CanvasLayer = $GameOverMenu

@onready var music: AudioStreamPlayer = $Music

@export var inventory_ui: InventoryUI

@export var inventory: Inventory

var _playing = true


func _ready():
	SignalBus.connect("player_killed", self.game_over)
	
	# Connect inventory to player
	inventory.left_gun_equipped.connect(stage.player.equip_gun_left)
	inventory.left_gun_unequipped.connect(stage.player.unequip_gun_left)
	
	inventory.right_gun_equipped.connect(stage.player.equip_gun_right)
	inventory.right_gun_unequipped.connect(stage.player.unequip_gun_right)
	
	# Create two guns and equip them by default
	if gun_default and gun_default.can_instantiate():
		var gun_1 := gun_default.instantiate() as Gun
		inventory.add_gun(gun_1)
		inventory.equip_gun_left(gun_1)
		
		var gun_2 := gun_default.instantiate() as Gun
		inventory.add_gun(gun_2)
		inventory.equip_gun_right(gun_2)
		
		var gun_3 := gun_default.instantiate() as Gun
		inventory.add_gun(gun_3)
	
	# Create default upgrades
	var upgrade_minigun_1 := Upgrade.new()
	upgrade_minigun_1.function = func(gun: Gun): gun.firerate /= 4
	upgrade_minigun_1.name = "Sasha"
	
	var upgrade_minigun_2 := Upgrade.new()
	upgrade_minigun_2.function = func(gun: Gun): gun.firerate /= 4
	upgrade_minigun_2.name = "Sasha"
	
	var upgrade_shotgun_1 := Upgrade.new()
	upgrade_shotgun_1.function = func(gun: Gun):
		gun.bullet_count += 5
		gun.firerate *= 2
		gun.spread = 15
	upgrade_shotgun_1.name = "Boink"
	
	var upgrade_shotgun_2 := Upgrade.new()
	upgrade_shotgun_2.function = func(gun: Gun):
		gun.bullet_count += 5
		gun.firerate *= 2
		gun.spread = 15
	upgrade_shotgun_2.name = "Boink"
	
	var upgrade_sniper_1 := Upgrade.new()
	upgrade_sniper_1.function = func(gun: Gun):
		gun.bullet_speed *= 1.5
		gun.pierce += 1
		gun.damage += 12
		gun.spread -= 5
	upgrade_sniper_1.name = "Good Job"
	
	var upgrade_sniper_2 = Upgrade.new()
	upgrade_sniper_2.function = func(gun: Gun):
		gun.bullet_speed *= 1.5
		gun.pierce += 1
		gun.damage += 12
		gun.spread -= 5
	upgrade_sniper_2.name = "Good Job"
	
	inventory.add_upgrade(upgrade_minigun_1)
	inventory.add_upgrade(upgrade_minigun_2)
	inventory.add_upgrade(upgrade_shotgun_1)
	inventory.add_upgrade(upgrade_shotgun_2)
	inventory.add_upgrade(upgrade_sniper_1)
	inventory.add_upgrade(upgrade_sniper_2)


func _process(_delta):
	if Input.is_action_just_pressed("pause"):
		self.toggle_pause()


func toggle_pause():
	if self._playing:
		if self.get_tree().paused:
			self.get_tree().paused = false
			self.pause_menu.hide()
		else:
			self.get_tree().paused = true
			self.pause_menu.show()


func main_menu():
	if is_instance_valid(self.main_menu_scene):
		self.get_tree().paused = false
		self.get_tree().change_scene_to_packed(main_menu_scene)


func game_over():
	self._playing = false
	self.get_tree().paused = true
	self.music.stop()
	self.game_over_menu.show()


func restart():
	self.get_tree().paused = false
	self.get_tree().change_scene_to_file(self.scene_file_path)


func quit():
	self.get_tree().quit()
