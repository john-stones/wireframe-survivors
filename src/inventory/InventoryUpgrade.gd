extends PanelContainer
class_name InventoryUpgrade


signal selected

@export_subgroup("internal")
@export
var _name_label: Label


func update(upgrade: Upgrade):
	_name_label.text = upgrade.name


func _gui_input(event):
	if event is InputEventMouseButton \
			and event.button_index == MOUSE_BUTTON_LEFT \
			and event.pressed:
		selected.emit()
