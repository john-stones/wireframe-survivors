extends Control
class_name InventoryUI


signal done

signal gun_equipped_left(gun: Gun)
signal gun_equipped_right(gun: Gun)

signal upgrade_equipped(gun: Gun, idx: int, upgrade: Upgrade)
signal upgrade_unequipped(gun: Gun, idx: int)

@export_subgroup("guns")
@export
var _guns_parent: Control

@export
var _gun_item_scene: PackedScene

## Dictionary[gun_instance_id: int, gun_ui: InventoryGun]
var _guns := {}

@export_subgroup("upgrades")
@export
var _upgrades_parent: Control

@export
var _upgrade_item_scene: PackedScene

## Dictionary[upgrade_instance_id: int, upgrade_ui: InventoryUpgrade]
var _upgrades := {}

var _upgrade_selected: Upgrade


func update(inventory: Inventory):
	# Remove existing guns that aren't in the inventory
	_guns.keys() \
		.map(func(gun_id): return instance_from_id(gun_id)) \
		.filter(func(gun): return not inventory.guns.has(gun)) \
		.map(func(gun): remove_gun(gun))
	
	# Create or update guns
	inventory.guns \
		.map(func(gun: Gun): return update_gun(gun)) \
		.map(func(gun_ui: InventoryGun):
			gun_ui.equipped = InventoryGun.EquippedStatus.NONE)
	
	# Set equip status
	if inventory.gun_equipped_left and get_gun(inventory.gun_equipped_left):
		get_gun(inventory.gun_equipped_left).equipped = \
			InventoryGun.EquippedStatus.LEFT
	
	if inventory.gun_equipped_right and get_gun(inventory.gun_equipped_right):
		get_gun(inventory.gun_equipped_right).equipped = \
			InventoryGun.EquippedStatus.RIGHT
	
	# Remove existing upgrades that aren't in the inventory
	_upgrades.keys() \
		.map(func(upgrade_id): return instance_from_id(upgrade_id)) \
		.filter(func(upgrade): return not inventory.upgrades.has(upgrade)) \
		.map(remove_upgrade)
	
	# Create or update upgrades
	inventory.upgrades.map(update_upgrade)


## Returns `null` if `gun` doesn't exist
func get_gun(gun: Gun) -> InventoryGun:
	return _guns.get(gun.get_instance_id())


func update_gun(gun: Gun) -> InventoryGun:
	var gun_item := get_gun(gun)
	
	# Create gun if id doesn't exist
	if gun_item == null:
		gun_item = _gun_item_scene.instantiate() as InventoryGun
		_guns[gun.get_instance_id()] = gun_item
		
		# Connect gun to autoupdate
		gun.updated.connect(update_gun)
		
		gun_item.selected_left.connect(func(): gun_equipped_left.emit(gun))
		gun_item.selected_right.connect(func(): gun_equipped_right.emit(gun))
		
		gun_item.upgrade_selected.connect(func(idx):
			upgrade_equipped.emit(gun, idx, _upgrade_selected))
		gun_item.upgrade_unequipped.connect(func(idx):
			upgrade_unequipped.emit(gun, idx))
		
		_guns_parent.add_child(gun_item)
	
	gun_item.update(gun)
	return gun_item


func remove_gun(gun: Gun):
	if not _guns.has(gun.get_instance_id()):
		# Do nothing
		return
	
	var gun_item := get_gun(gun)
	gun_item.queue_free()
	
	_guns.erase(gun)


func clear_guns():
	_guns.keys().map(func(gun): remove_gun(gun))


func get_upgrade(upgrade: Upgrade) -> InventoryUpgrade:
	return _upgrades.get(upgrade.get_instance_id())


func update_upgrade(upgrade: Upgrade) -> InventoryUpgrade:
	var upgrade_item := get_upgrade(upgrade)
	
	if upgrade_item == null:
		# Create upgrade UI item
		upgrade_item = _upgrade_item_scene.instantiate()
		_upgrades[upgrade.get_instance_id()] = upgrade_item
		
		upgrade_item.selected.connect(select_upgrade.bind(upgrade))
		
		_upgrades_parent.add_child(upgrade_item)
	
	upgrade_item.update(upgrade)
	return upgrade_item


func remove_upgrade(upgrade: Upgrade):
	if not _upgrades.has(upgrade.get_instance_id()):
		# Do nothing
		return
	
	var upgrade_item = get_upgrade(upgrade)
	upgrade_item.queue_free()
	
	_upgrades.erase(upgrade.get_instance_id())


func clear_upgrades():
	_upgrades.keys.map(func(upgrade): remove_upgrade(upgrade))


func select_upgrade(upgrade: Upgrade):
	_upgrade_selected = upgrade


func clear_selected_upgrade():
	_upgrade_selected = null
