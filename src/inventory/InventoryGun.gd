extends Control
class_name InventoryGun


signal upgrade_selected(idx: int)
signal upgrade_unequipped(idx: int)

signal selected_left()
signal selected_right()

const _TESTING = false

enum EquippedStatus { NONE, LEFT, RIGHT }

@export
var _name: Label

@export
var _equipped_label: Label

var equipped := EquippedStatus.NONE:
	set(value):
		equipped = value
		match equipped:
			EquippedStatus.NONE:
				_equipped_label.text = ""
			EquippedStatus.LEFT:
				_equipped_label.text = "Left"
			EquippedStatus.RIGHT:
				_equipped_label.text = "Right"

var _upgrades_ui: Array[Control]


func update(gun: Gun):
	_name.text = gun.display_name
	
	# Recreate upgrades
	_clear_upgrades()
	
	range(gun.UPGRADE_COUNT_MAX).map(func(idx):
		var upgrade_ui := _create_upgrade()
		if idx < gun._upgrades.size():
			var upgrade := gun._upgrades[idx]
			if upgrade:
				upgrade_ui.text = upgrade.name
		
		upgrade_ui.gui_input.connect(_upgrade_gui_input.bind(idx))
			
		_upgrades_ui.append(upgrade_ui)
		_name.add_sibling(upgrade_ui))


func set_equipped(status: EquippedStatus):
	match status:
		EquippedStatus.NONE:
			_equipped_label.text = ""
		EquippedStatus.LEFT:
			_equipped_label.text = "Left"
		EquippedStatus.RIGHT:
			_equipped_label.text = "Right"


func _clear_upgrades():
	_upgrades_ui.map(func(upgrade): upgrade.queue_free())
	_upgrades_ui.clear()


func _create_upgrade() -> Control:
	var upgrade_ui := Button.new()
	upgrade_ui.custom_minimum_size = Vector2(32, 32)
	upgrade_ui.size_flags_horizontal = Control.SIZE_SHRINK_BEGIN
	upgrade_ui.size_flags_vertical = Control.SIZE_SHRINK_CENTER
	upgrade_ui.focus_mode = Control.FOCUS_NONE
	return upgrade_ui


func _upgrade_gui_input(event: InputEvent, idx: int):
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
			upgrade_selected.emit(idx)
		if event.button_index == MOUSE_BUTTON_RIGHT and event.pressed:
			upgrade_unequipped.emit(idx)


func _gun_gui_input(event: InputEvent):
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
			selected_left.emit()
		if event.button_index == MOUSE_BUTTON_RIGHT and event.pressed:
			selected_right.emit()
