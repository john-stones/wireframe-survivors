extends Node
class_name Inventory


signal updated(inventory: Inventory)

signal left_gun_equipped(gun: Gun)
signal left_gun_unequipped()

signal right_gun_equipped(gun: Gun)
signal right_gun_unequipped()

var guns: Array[Gun] = []

var gun_equipped_left: Gun
var gun_equipped_right: Gun

var upgrades: Array[Upgrade] = []


func _ready():
	updated.emit(self)


func add_gun(gun: Gun):
	self.guns.append(gun)
	updated.emit(self)


func remove_gun(gun: Gun):
	if gun_equipped_left == gun:
		unequip_gun_left()
	if gun_equipped_right == gun:
		unequip_gun_right()
	self.guns.erase(gun)
	updated.emit(self)


func equip_gun_left(gun: Gun):
	if not guns.has(gun):
		return
	
	# If already equipped, do nothing
	if gun_equipped_left == gun:
		return
	
	# If equipped in other hand, swap hands
	if gun_equipped_right == gun:
		if is_instance_valid(gun_equipped_left):
			var unequipped_gun := unequip_gun_left()
			equip_gun_right(unequipped_gun)
		else:
			unequip_gun_right()
	
	gun_equipped_left = gun
	left_gun_equipped.emit(gun)
	updated.emit(self)


func unequip_gun_left() -> Gun:
	var unequipped_gun := gun_equipped_left as Gun
	
	if gun_equipped_left != null:
		gun_equipped_left = null
		left_gun_unequipped.emit()
		updated.emit(self)
	
	return unequipped_gun


func equip_gun_right(gun: Gun):
	if not guns.has(gun):
		return
	
	if gun_equipped_right == gun:
		return
	
	if gun_equipped_left == gun:
		if is_instance_valid(gun_equipped_right):
			var unequipped_gun := unequip_gun_right()
			equip_gun_left(unequipped_gun)
		else:
			unequip_gun_left()
	
	gun_equipped_right = gun
	right_gun_equipped.emit(gun)
	updated.emit(self)


func unequip_gun_right() -> Gun:
	var unequipped_gun := gun_equipped_right as Gun
	
	if gun_equipped_right != null:
		gun_equipped_right = null
		right_gun_unequipped.emit()
		updated.emit(self)
	
	return unequipped_gun


func add_upgrade(upgrade: Upgrade):
	var upgrade_idx := upgrades.find(upgrade)
	
	if upgrade_idx >= 0:
		upgrades[upgrade_idx] = upgrade
	else:
		upgrades.append(upgrade)
	
	updated.emit(self)


func remove_upgrade(upgrade: Upgrade):
	upgrades.erase(upgrade)
	updated.emit(self)


func equip_upgrade(gun: Gun, idx: int, upgrade: Upgrade):
	if not upgrades.has(upgrade): return
	
	remove_upgrade(upgrade)
	gun.add_upgrade(idx, upgrade)
	
	updated.emit(self)


func unequip_upgrade(gun: Gun, idx: int):
	var upgrade := gun.get_upgrade(idx)
	
	if upgrade != null and upgrade.function != Callable(Upgrade, "default_function"):
		gun.remove_upgrade(idx)
		add_upgrade(upgrade)
		
		updated.emit(self)
