extends Resource
class_name Upgrade


var name := ""

var description := ""

var function := default_function


static func of(function_in: Callable) -> Upgrade:
	var upgrade := Upgrade.new()
	upgrade.function = function_in
	return upgrade


func apply(gun: Gun):
	self.function.call(gun)


static func default_function(_gun: Gun):
	pass
