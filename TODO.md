# Wireframe Survivors To Do List

A list of things to do for minimum viable product (MVP) of Wireframe Survivors.

Requirements are described using the key words 'REQUIRED', 'RECOMMENDED', and
'OPTIONAL' as per [BCP 14][bcp_14]. Anything followed by '...' can only be
marked as complete if its children are satisfactorily completed. Anything
preceded by '---' is a comment.

## The List

* [ ] Gameplay ...
	* [ ] Powerups ...
		* [ ] REQUIRED Magnet all experience
		* [ ] REQUIRED Double gun
		* [ ] REQUIRED Clear screen
		* [ ] REQUIRED Fan of bullets around player
		* [ ] OPTIONAL Whatever other powerups
	* [ ] OPTIONAL More types of gun
		* [ ] REQUIREMENT Way to choose gun

* [ ] Enemies ...
	* [X] REQUIRED More types of enemies (3?)
	* [ ] OPTIONAL Give enemies a health bar

* [ ] Better stage design ...
	* [ ] REQUIRED Stage progression
		* --- Most obvious is the stage getting harder over time but may be
			another way to make stages have some progression.
	* [ ] OPTIONAL More than one stage
		[ ] REQUIRED Way to change stages
	* [ ] OPTIONAL Static obstacles inside the world boundry

* [ ] Sound ...
	* [ ] REQUIRED Main music track
	* [ ] OPTIONAL Multiple music tracks for different scenes
	* [ ] Sound FX ...
		* [ ] REQUIRED Gun fired sound
		* [ ] REQUIRED Enemy killed sound
		* [ ] REQUIRED Player killed sound
		* [ ] REQUIRED Pickup collectable sound
		* [ ] OPTIONAL Separate sounds for different collectables
		* [ ] RECOMMENDED Pause sound
		* [ ] RECOMMENDED Unpause sound
		* [ ] RECOMMENDED Menu button hover sound
		* [ ] RECOMMENDED Menu button select sound
		* [ ] OPTIONAL Bullet hit wall sound

* [ ] Better visuals ...
	* [ ] RECOMMENDED Reactive animations
	* [ ] RECOMMENDED More interesting backgrounds

[bcp_14]: https://www.rfc-editor.org/info/bcp14
