# Wireframe Survivors

## Attributions

* Music by [Abstraction](http://www.abstractionmusic.com)
* Some sound effects by [Kronbits](https://kronbits.itch.io/freesfx)
* Some sound effects by [Kenney](https://kenney.itch.io/kenney-game-assets-1)
